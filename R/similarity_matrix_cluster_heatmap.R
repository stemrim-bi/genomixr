#' Similarity matrix heatmap
#'
#' Create a heatmap based on similarity matrix (correlation).
#'
#' @param mat   a matrix, usually output of cor()
#' @param main_title main title to be included in plotting image
#' @param fontsize size of the fonts in the image
#' @param color_range number of color used for heatmap, the higher the more refined in encoding the values
#' @param outfile if no output file is given, the plot will be shown on the fly
#' @param width  width of the image saved, inherit from png()
#' @param height height of the image saved, inherit from png()
#' @param color_palette  heatmap color palette one of: 'viridis', 'inferno' and 'magma'
#' @param res image resolution
#'
#' @return pheatmap object or PNG file
#' @export
#'
#' @examples
#' df <- cor(mtcars,mtcars)
#' p <- plot_sim_heatmap(df, main_title = "", color_range=10000)
plot_sim_heatmap <- function(mat, main_title=NULL, fontsize=12,
                             color_palette="inferno",
                             color_range=100, outfile=NULL, width=480, height=480, res=300) {
  color_vector <- NULL
  if (color_palette == "inferno") {
    color_vector <- viridis::inferno(color_range)
  } else if (color_palette == "magma") {
    color_vector <- viridis::magma(color_range)
  } else if (color_palette == "viridis") {
    color_vector <- viridis::viridis(color_range)
  }
  p <- pheatmap::pheatmap(
    mat,
    color = color_vector,
    border_color = NA,
    show_colnames = TRUE,
    show_rownames = TRUE,
    # annotation_col    = mat_col,
    # annotation_colors = mat_colors,
    clustering_distance_rows = "correlation", # This default to pearson
    clustering_distance_cols = "correlation",
    clustering_method = "ward.D2",
    drop_levels = TRUE,
    fontsize = fontsize,
    main = main_title
  )
  if (!is.null(outfile)) {
    save_pheatmap_png(p, outfile, width = width, height = height, res = res)
  }
}


#' Saving pheatmap output as PNG
#'
#' This is a helper function for saving \code{\link{pheatmap}} output as file.
#' It's needed because it doesn't output a plotting object.
#'
#' @param p     a \code{\link{pheatmap}} object (which is not the same as standard plotting object)
#' @param filename output file name
#' @param width  width of the image saved, later passed to \code{\link{png}}
#' @param height height of the image saved, later passed to \code{\link{png}}
#' @param res image resolution
#' @param ... further arguments inherited from \code{\link{png}}
#'
#' @return PNG image as a file
#' @export
#'
#' @seealso \code{\link{pheatmap}}
#'
#' @examples
#' save_pheatmap_png(p, "outfile.png", width=480, height=480)
save_pheatmap_png <- function(p, filename, width=7, height=7, res=300, ...) {
  stopifnot(!missing(p))
  stopifnot(!missing(filename))

  png(filename, width = width, height = height, res = res, ...)
  grid::grid.newpage()
  grid::grid.draw(p$gtable)
  dev.off()
}
