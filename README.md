# R package: "genomixr"


## Description

The `genomixr` package is the package associated with various bioinformatics data analysis conducted in Genomix Co. Ltd. The package contains a number of convenience plotting functions and simple machine learning tools, such as clustering, etc that I've found are handy to use day-to-day. 


## Installation

### From bitbucket

The `devtools` package contains functions that allow you to install R packages directly from bitbucket or github. If you've installed and loaded the `devtools` package, the installation command is

`devtools::install_bitbucket("genomix-bi/genomixr",dependencies=TRUE)`

## Package contents

Each function can be found in a separate file, with the usual .R extension. Some minimal documentation and commenting can be found in the source code, but as usual the most extensive help information is in the .Rd file associated with each function.

### Plotting functions

- `plot_clust_dendro` Dendrogram plots based on hierarchical clustering
- `pretty_scatter_corrplot` A pretty scatter plot for correlation analysis
- `plot_tsne_pca` Clustering plot based on tSNE and PCA
- `plot_kmeans_heatmap` A heatmap where the rows are clustered using K-means
- `plot_venn2sets_with_pvalue` Two-sets Venn diagram with p-value added

